package lib.utils;

/**
 * Key-value pair container
 *
 * @param <K> key type
 * @param <V> value type
 */
public class KeyValuePair<K, V>
{
    private final K key;
    private final V value;

    /**
     * Constructor
     *
     * @param key key
     * @param value value
     */
    public KeyValuePair(K key, V value)
    {
        this.key = key;
        this.value = value;
    }

    /**
     * Get key
     *
     * @return key
     */
    public K getKey()
    {
        return key;
    }

    /**
     * Get value
     *
     * @return value
     */
    public V getValue()
    {
        return value;
    }
}

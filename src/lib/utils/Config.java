package lib.utils;

import exceptions.handlers.ConfigExceptionHandler;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Config provider
 */
public class Config
{
    private static Config config = null;
    private final Properties properties;

    /**
     * Constructor
     */
    private Config()
    {
        properties = new Properties();

        try {
            properties.load(
                new FileInputStream("./config/config.properties")
            );
        } catch (FileNotFoundException exception) {
            ConfigExceptionHandler.showConfigExceptionDialog("Config couldn't be found!");
        } catch (IOException exception) {
            ConfigExceptionHandler.showConfigExceptionDialog("Config couldn't be loaded!");
        }
    }

    /**
     * Get config property
     *
     * @param property property name
     * @return value of the config property
     */
    public static Object get(String property)
    {
        return getInstance().getProperty(property);
    }

    /**
     * Get property from the Properties object
     *
     * @param property property name
     * @return value of the config property
     */
    private Object getProperty(String property)
    {
        return properties.getProperty(property);
    }

    /**
     * Get instance of the Config class
     *
     * @return instance of the Config class
     */
    private static Config getInstance()
    {
        return (config == null) ? (config = new Config()) : config;
    }
}

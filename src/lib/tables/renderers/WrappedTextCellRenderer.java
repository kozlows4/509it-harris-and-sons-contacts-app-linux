package lib.tables.renderers;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Renderer to display wrapped text
 */
public class WrappedTextCellRenderer extends JTextArea implements TableCellRenderer
{
    /**
     * Constructor
     */
    public WrappedTextCellRenderer()
    {
        setOpaque(true);
        setLineWrap(false);
        setWrapStyleWord(true);
    }

    @Override
    public Component getTableCellRendererComponent(
        JTable table,
        Object value,
        boolean isSelected,
        boolean hasFocus,
        int row,
        int column
    )
    {
        this.setText((value == null) ? "" : value.toString());

        this.setBackground(isSelected ? Color.decode("#b8cfe5") : Color.WHITE);

        return this;
    }
}


package lib.tables.models;

import lib.tables.helpers.AddressCellSettings;
import lib.tables.helpers.TableRowsSettings;
import lib.utils.KeyValuePair;

import java.util.ArrayList;

public class BusinessContactsTableModel extends ContactsListTableModel
{
    /**
     * IDs matching row indexes
     */
    private Integer[] rowIDs;

    /**
     * Constructor
     */
    public BusinessContactsTableModel()
    {
        columnNames = new String[]{
            "First Name",
            "Last Name",
            "Email",
            "Phone",
            "Address",
            "Postcode",
            "City",
            "Country",
            "Business Phone",
            "Company",
            "Position"
        };

        data = new Object[][]{};
    }

    @Override
    public int getRowId(int index)
    {
        return rowIDs[index];
    }

    @Override
    protected TableRowsSettings formatTableData(ArrayList<String[]> rows, int perPage)
    {
        int rowsCount = rows.size();
        int displayedRowsCount = Math.min(rowsCount, perPage);

        Object[][] newTableRows = new Object[displayedRowsCount][getColumnCount()];
        ArrayList<KeyValuePair<Integer, Integer>> rowsHeightSettings = new ArrayList<>();
        Integer[] newRowIDs = new Integer[displayedRowsCount];

        for (int i = 0; i < displayedRowsCount; i++)
        {
            String[] resultRow = rows.get(i);

            newRowIDs[i] = Integer.valueOf(resultRow[0]);

            newTableRows[i][0] = resultRow[1];
            newTableRows[i][1] = resultRow[2];
            newTableRows[i][2] = resultRow[3];
            newTableRows[i][3] = formatPhoneNumber(resultRow[4], resultRow[5]);

            AddressCellSettings addressCellSettings = formatAddress(i, resultRow[6], resultRow[7], resultRow[8]);
            rowsHeightSettings.add(addressCellSettings.rowHeightSettings);

            newTableRows[i][4] = addressCellSettings.address;
            newTableRows[i][5] = resultRow[9];
            newTableRows[i][6] = resultRow[10];
            newTableRows[i][7] = resultRow[11];
            newTableRows[i][8] = formatPhoneNumber(resultRow[12], resultRow[13]);
            newTableRows[i][9] = resultRow[14];
            newTableRows[i][10] = resultRow[15];
        }

        rowIDs = newRowIDs;

        return new TableRowsSettings(
            newTableRows,
            rowsHeightSettings,
            rowsCount,
            displayedRowsCount
        );
    }
}

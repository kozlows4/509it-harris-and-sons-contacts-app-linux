package lib.tables.models;

import lib.db.QueryResult;
import lib.tables.helpers.AddressCellSettings;
import lib.tables.helpers.TableRowsSettings;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

/**
 * Contacts list TableModel
 */
public abstract class ContactsListTableModel extends AbstractTableModel
{
    /**
     * Column names
     */
    protected String[] columnNames;

    /**
     * Table data
     */
    protected Object[][] data;

    @Override
    public int getColumnCount()
    {
        return columnNames.length;
    }

    @Override
    public int getRowCount()
    {
        return data.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        return data[rowIndex][columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        return false;
    }

    @Override
    public String getColumnName(int column)
    {
        return columnNames[column];
    }

    /**
     * Update table data
     *
     * @param result data retrieved from the database
     * @param perPage maximum number of rows to be displayed
     * @return number of retrieved rows
     */
    public TableRowsSettings updateData(QueryResult result, int perPage)
    {
        TableRowsSettings rowsSettings = formatTableData(result.rows, perPage);

        data = rowsSettings.tableData;

        this.fireTableDataChanged();

        return rowsSettings;
    }

    /**
     * Get ID of the selected row
     *
     * @param index index of the selected row
     * @return ID of the selected row
     */
    public abstract int getRowId(int index);

    /**
     * Format table data
     *
     * @param rows data retrieved from the database
     * @param perPage maximum number of rows to be displayed
     * @return formatted data
     */
    protected abstract TableRowsSettings formatTableData(ArrayList<String[]> rows, int perPage);

    /**
     * Format phone number
     *
     * @param areaCode area code
     * @param phoneNumber phone number
     * @return formatted phone number
     */
    protected static String formatPhoneNumber(String areaCode, String phoneNumber)
    {
        if (areaCode.equals("0")) {
            return areaCode + phoneNumber;
        } else {
            return "+" + areaCode + " " + phoneNumber;
        }
    }

    /**
     * Format address
     *
     * @param addressLine1 first address line
     * @param addressLine2 second address line
     * @param addressLine3 third address line
     * @return formatted address
     */
    protected static AddressCellSettings formatAddress(int rowNumber, String addressLine1, String addressLine2, String addressLine3)
    {
        int addressLines = 1;
        String address = addressLine1;

        if (addressLine2 != null && addressLine2.length() > 0) {
            address += (System.lineSeparator() + addressLine2);
            addressLines++;
        }

        if (addressLine3 != null && addressLine3.length() > 0) {
            address += (System.lineSeparator() + addressLine3);
            addressLines++;
        }

        return new AddressCellSettings(
            rowNumber,
            address,
            addressLines
        );
    }
}

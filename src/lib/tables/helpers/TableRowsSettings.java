package lib.tables.helpers;

import lib.utils.KeyValuePair;

import java.util.ArrayList;

public class TableRowsSettings
{
    public int retrievedRowsCount;
    public int displayedRowsCount;
    public Object[][] tableData;
    public ArrayList<KeyValuePair<Integer, Integer>> rowsHeightSettings;

    /**
     * Constructor
     *
     * @param tableData data used to populate the table
     * @param rowsHeightSettings displayed rows settings
     */
    public TableRowsSettings(
        Object[][] tableData,
        ArrayList<KeyValuePair<Integer, Integer>> rowsHeightSettings,
        int retrievedRowsCount,
        int displayedRowsCount
    )
    {
        this.tableData = tableData;
        this.rowsHeightSettings = rowsHeightSettings;
        this.retrievedRowsCount = retrievedRowsCount;
        this.displayedRowsCount = displayedRowsCount;
    }

    /**
     * Check if next page can be displayed
     *
     * @return information if next page can be displayed
     */
    public boolean hasNextPage()
    {
        return retrievedRowsCount > displayedRowsCount;
    }
}

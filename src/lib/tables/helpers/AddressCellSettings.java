package lib.tables.helpers;

import lib.utils.KeyValuePair;

public class AddressCellSettings
{
    public int rowNumber;
    public KeyValuePair<Integer, Integer> rowHeightSettings;
    public String address;

    private static final int baseRowHeight = 20;

    /**
     * Constructor
     *
     * @param rowNumber row number
     * @param address formatted address
     * @param addressLines number of address lines
     */
    public AddressCellSettings(int rowNumber, String address, int addressLines)
    {
        this.rowNumber = rowNumber;
        this.address = address;
        this.rowHeightSettings = new KeyValuePair<>(rowNumber, baseRowHeight * addressLines);
    }
}

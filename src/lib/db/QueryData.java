package lib.db;

import lib.utils.KeyValuePair;

import java.util.ArrayList;

/**
 * Query data container
 */
public class QueryData
{
    public String query;
    public ArrayList<KeyValuePair<String, Object>> parameters;

    /**
     * Constructor
     *
     * @param query query string
     * @param parameters parameters
     */
    public QueryData(String query, ArrayList<KeyValuePair<String, Object>> parameters)
    {
        this.query = query;
        this.parameters = parameters;
    }
}

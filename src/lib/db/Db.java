package lib.db;

import exceptions.handlers.DbExceptionHandler;
import lib.utils.Config;
import lib.utils.KeyValuePair;

import java.sql.*;

/**
 * Database connection
 */
public class Db
{
    /**
     * DB connection settings
     */
    private static final String connectionString = "jdbc:mysql://" +
        Config.get("server") +
        ":" +
        Config.get("port") +
        "/" +
        Config.get("database");
    private static final String dbUser = Config.get("user").toString();
    private static final String dbPassword = Config.get("password").toString();

    /**
     * Get data from the database
     *
     * @param queryData query string
     * @return selected data
     */
    public static QueryResult query(QueryData queryData)
    {
        try (Connection connection = connect()) {
            if (connection == null) {
                throw new NullPointerException();
            }

            return new QueryResult(
                prepareStatement(connection, queryData).executeQuery()
            );
        } catch (NullPointerException exception) {
            DbExceptionHandler.showDbExceptionDialog("Couldn't connect to the database!");

            return null;
        } catch (SQLException exception) {
            DbExceptionHandler.showDbExceptionDialog("Invalid query!");

            return null;
        }
    }

    /**
     * Perform operation other than SELECT
     *
     * @param queryData query string
     * @return result of the query
     */
    public static boolean nonQuery(QueryData queryData)
    {
        try (Connection connection = connect()) {
            if (connection == null) {
                throw new NullPointerException();
            }

            return prepareStatement(connection, queryData).executeUpdate() > 0;
        } catch (NullPointerException exception) {
            DbExceptionHandler.showDbExceptionDialog("Couldn't connect to the database!");

            return false;
        } catch (SQLException exception) {
            DbExceptionHandler.showDbExceptionDialog("Invalid query!");

            return false;
        }
    }

    /**
     * Connect to the database
     *
     * @return Connection instance
     */
    private static Connection connect()
    {
        try {
            return DriverManager.getConnection(connectionString, dbUser, dbPassword);
        } catch (SQLException exception) {
            DbExceptionHandler.showDbExceptionDialog("Invalid database configuration!");

            return null;
        }
    }

    /**
     * Prepare statement
     *
     * @param connection connection
     * @param queryData query data
     * @return prepared statement
     * @throws SQLException exception
     */
    private static PreparedStatement prepareStatement(Connection connection, QueryData queryData) throws SQLException
    {
        PreparedStatement statement = connection.prepareStatement(queryData.query);

        int j = 1;

        for (KeyValuePair<String, Object> parameter : queryData.parameters)
        {
            switch (parameter.getKey()) {
                case "string" -> statement.setString(j, parameter.getValue().toString());
                case "int" -> statement.setInt(j, (Integer) parameter.getValue());
                case "null" -> statement.setNull(j, Types.NULL);
            }

            j++;
        }

        return statement;
    }
}

package lib.db;

import exceptions.handlers.DbExceptionHandler;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Query result container
 */
public class QueryResult
{
    public ArrayList<String[]> rows = new ArrayList<>();
    public int columnsCount = 0;
    public String[] columnNames = {};

    /**
     * Constructor
     *
     * @param resultSet data retrieved from the database
     */
    public QueryResult(ResultSet resultSet)
    {
        try {
            ResultSetMetaData metaData = resultSet.getMetaData();

            columnsCount = metaData.getColumnCount();
            columnNames = getColumnNames(metaData);
            rows = getRows(resultSet);
        } catch (SQLException exception) {
            DbExceptionHandler.showQueryExceptionDialog("Database query failed!");
        }
    }

    /**
     * Get column names
     *
     * @param metaData ResultSet's metadata
     * @return list of column names
     * @throws SQLException metadata loop exception
     */
    private String[] getColumnNames(ResultSetMetaData metaData) throws SQLException
    {
        String[] dbColumnNames = new String[columnsCount];

        for (int i = 0, j = 1; j <= columnsCount; i++, j++) {
            dbColumnNames[i] = metaData.getColumnName(j);
        }

        return dbColumnNames;
    }

    /**
     * Get data from the ResultSet
     *
     * @param resultSet data retrieved from the database
     * @return formatted data retrieved from the database
     * @throws SQLException ResultSet loop exception
     */
    private ArrayList<String[]> getRows(ResultSet resultSet) throws SQLException
    {
        ArrayList<String[]> dbRows = new ArrayList<>();

        while (resultSet.next()) {
            String[] row = new String[columnsCount];

            for (int i = 0, j = 1; j <= columnsCount; i++, j++)
            {
                row[i] = resultSet.getString(j);
            }

            dbRows.add(row);
        }

        return dbRows;
    }
}

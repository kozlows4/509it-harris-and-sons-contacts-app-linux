package lib.models;

import lib.db.Db;
import lib.db.QueryData;
import lib.db.QueryResult;
import lib.models.helpers.SearchData;
import lib.utils.KeyValuePair;

import java.util.ArrayList;


/**
 * Database access model
 */
public abstract class Model
{
    /**
     * Sorting order
     */
    private final static String ascending = "ASC";
    private final static String descending = "DESC";

    /**
     * Table used in queries
     */
    protected String table;

    /**
     * Primary key column
     */
    protected String idColumn;

    /**
     * Columns used to filter data
     */
    protected String[] searchableColumns;

    /**
     * Columns used to sort the results set
     */
    protected ArrayList<KeyValuePair<Integer, String>> orderableColumns;

    /**
     * Get data from the database
     *
     * @param search search string
     * @param limit limit value
     * @param offset offset value
     * @param orderColumn column used to sort data
     * @param sortAscending sorting direction
     * @return selected data
     */
    public QueryResult get(String search, int limit, int offset, int orderColumn, boolean sortAscending)
    {
        return Db.query(
            getSelectQueryData(search, limit, offset, orderColumn, sortAscending)
        );
    }

    /**
     * Get single row from the database
     *
     * @param id row ID
     * @return selected row
     */
    public QueryResult find(int id)
    {
        return Db.query(
            getSelectQueryData(id)
        );
    }

    /**
     * Delete contact
     *
     * @param id contact ID
     * @return DELETE query result
     */
    public boolean delete(int id)
    {
        return Db.nonQuery(
            getDeleteQueryData(id)
        );
    }

    /**
     * Add new contact
     *
     * @param values values to be added
     * @return INSERT query result
     */
    public boolean insert(ArrayList<KeyValuePair<String, Object>> values)
    {
        return Db.nonQuery(
            getInsertQueryData(values)
        );
    }

    /**
     * Update existing contact
     *
     * @param id contact ID
     * @param values values to be updated
     * @return UPDATE query result
     */
    public boolean update(int id, ArrayList<KeyValuePair<String, Object>> values)
    {
        return Db.nonQuery(
            getUpdateQueryData(id, values)
        );
    }

    /**
     * Get list of searchable columns
     *
     * @return searchable columns
     */
    protected abstract String[] getSearchableColumns();

    /**
     * Get single contact from the database
     *
     * @param id contact id
     * @return prepared query data
     */
    private QueryData getSelectQueryData(int id)
    {
        ArrayList<KeyValuePair<String, Object>> parameters = new ArrayList<>(1);
        parameters.add(
            new KeyValuePair<>("int", id)
        );

        return new QueryData(
            "SELECT * FROM " + table + " WHERE " + idColumn + " = ? LIMIT 1",
            parameters
        );
    }

    /**
     * Get multiple contacts from the database
     *
     * @param search search string
     * @param limit limit value
     * @param offset offset value
     * @param orderColumn column used to sort data
     * @param sortAscending sorting direction
     * @return prepared query data
     */
    private QueryData getSelectQueryData(String search, int limit, int offset, int orderColumn, boolean sortAscending)
    {
        SearchData searchData = getSearchData(search);
        ArrayList<KeyValuePair<String, Object>> parameters = new ArrayList<>(1);

        for (String parameter : searchData.parameters)
        {
            parameters.add(new KeyValuePair<>("string", parameter));
        }

        parameters.add(new KeyValuePair<>("int", limit));
        parameters.add(new KeyValuePair<>("int", offset));

        return new QueryData(
            "SELECT * FROM " +
                table +
                " " +
                searchData.searchString +
                " ORDER BY " +
                getOrderByColumn(orderColumn) +
                " " +
                getSortingOrder(sortAscending) +
                " LIMIT ? OFFSET ?",
            parameters
        );
    }

    /**
     * Get DELETE query data
     *
     * @param id contact ID
     * @return query data
     */
    private QueryData getDeleteQueryData(int id)
    {
        ArrayList<KeyValuePair<String, Object>> parameters = new ArrayList<>(1);
        parameters.add(
            new KeyValuePair<>("int", id)
        );

        return new QueryData(
            "DELETE FROM " + table + " WHERE " + idColumn + " = ?",
            parameters
        );
    }

    /**
     * Get INSERT query data
     *
     * @param values values to be inserted
     * @return query data
     */
    private QueryData getInsertQueryData(ArrayList<KeyValuePair<String, Object>> values)
    {
        ArrayList<KeyValuePair<String, Object>> parameters = new ArrayList<>(values.size());
        StringBuilder queryColumns = new StringBuilder(" (" + idColumn);
        StringBuilder queryValues = new StringBuilder(" (NULL");

        for (KeyValuePair<String, Object> value : values)
        {
            queryColumns.append(", ").append(value.getKey());
            queryValues.append(", ?");

            parameters.add(
                new KeyValuePair<>(
                    String.valueOf(
                        value.getValue()
                    ).length() > 0 ? "string" : "null",
                    value.getValue()
                )
            );
        }

        return new QueryData(
            "INSERT INTO " + table + queryColumns + ") VALUES " + queryValues + ")",
            parameters
        );
    }

    /**
     * Get UPDATE query data
     *
     * @param id ID of the contact to be updated
     * @param values values to be updated
     * @return query data
     */
    private QueryData getUpdateQueryData(int id, ArrayList<KeyValuePair<String, Object>> values)
    {
        ArrayList<KeyValuePair<String, Object>> parameters = new ArrayList<>(values.size() + 1);
        StringBuilder query = new StringBuilder("UPDATE " + table + " SET ");

        boolean isFirstValue = true;

        for (KeyValuePair<String, Object> value : values)
        {
            if (isFirstValue) {
                isFirstValue = false;

                query.append(value.getKey()).append(" = ?");
            } else {
                query.append(", ").append(value.getKey()).append(" = ?");
            }

            parameters.add(
                new KeyValuePair<>(
                    value.getValue() != null ? "string" : "null",
                    value.getValue()
                )
            );
        }

        parameters.add(new KeyValuePair<>("int", id));

        return new QueryData(
            query + " WHERE " + idColumn + " = ?",
            parameters
        );
    }

    /**
     * Get search data
     *
     * @param search search string
     * @return search data
     */
    private SearchData getSearchData(String search)
    {
        StringBuilder searchString = new StringBuilder();
        ArrayList<String> parameters = new ArrayList<>();

        if (search.length() > 0) {
            searchString.append(" WHERE ");

            for (int i = 0, j = searchableColumns.length; i < j; i++) {
                if (i == 0) {
                    searchString.append(searchableColumns[i]).append(" LIKE CONCAT(\"%\", ?, \"%\")");
                } else {
                    searchString.append(" OR ").append(searchableColumns[i]).append(" LIKE CONCAT(\"%\", ?, \"%\")");
                }

                parameters.add(search);
            }
        }

        return new SearchData(searchString.toString(), parameters);
    }

    /**
     * Get sorting direction
     *
     * @param sortAscending sort ascending flag
     * @return sorting direction
     */
    private String getSortingOrder(boolean sortAscending)
    {
        return sortAscending ? ascending : descending;
    }

    /**
     * Get ORDER BY column
     *
     * @param orderColumn column id
     * @return column name
     */
    private String getOrderByColumn(int orderColumn)
    {
        String column = idColumn;

        for (KeyValuePair<Integer, String> orderableColumn : orderableColumns) {
            if (orderableColumn.getKey() == orderColumn) {
                column = orderableColumn.getValue();

                break;
            }
        }

        return column;
    }
}

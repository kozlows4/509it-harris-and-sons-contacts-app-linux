package lib.models;

public class PersonalContact extends Contact
{
    /**
     * Constructor
     */
    public PersonalContact()
    {
        table = "personal_contacts";
        idColumn = "id";
        ownSearchableColumns = new String[] {
            "home_phone_area_code",
            "home_phone_number"
        };
        searchableColumns = getSearchableColumns();
    }
}

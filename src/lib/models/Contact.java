package lib.models;

import lib.utils.KeyValuePair;

import java.util.ArrayList;

public abstract class Contact extends Model
{
    /**
     * Searchable columns shared by all tables
     */
    protected static final String[] sharedSearchableColumns = {
        "first_name",
        "last_name",
        "email",
        "phone_area_code",
        "phone_number",
        "address_line_1",
        "address_line_2",
        "address_line_3",
        "postcode",
        "city",
        "country"
    };

    /**
     * Search fields belonging solely to the model
     */
    protected String[] ownSearchableColumns;

    /**
     * Constructor
     */
    public Contact()
    {
        orderableColumns = new ArrayList<>();
        orderableColumns.add(new KeyValuePair<>(0, "first_name"));
        orderableColumns.add(new KeyValuePair<>(1, "last_name"));
        orderableColumns.add(new KeyValuePair<>(2, "email"));
    }

    @Override
    protected String[] getSearchableColumns()
    {
        String[] searchableColumns = new String[sharedSearchableColumns.length + ownSearchableColumns.length];

        int i = 0;

        for (String column : sharedSearchableColumns)
        {
            searchableColumns[i++] = column;
        }

        for (String column : ownSearchableColumns)
        {
            searchableColumns[i++] = column;
        }

        return searchableColumns;
    }
}

package lib.models.helpers;

import java.util.ArrayList;

/**
 * Search data container
 */
public class SearchData
{
    public String searchString;
    public ArrayList<String> parameters;

    /**
     * Constructor
     *
     * @param searchString search string
     * @param parameters parameters
     */
    public SearchData(String searchString, ArrayList<String> parameters)
    {
        this.searchString = searchString;
        this.parameters = parameters;
    }
}

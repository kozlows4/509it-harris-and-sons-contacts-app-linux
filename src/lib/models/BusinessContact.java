package lib.models;

import lib.utils.KeyValuePair;

public class BusinessContact extends Contact
{
    /**
     * Constructor
     */
    public BusinessContact()
    {
        table = "business_contacts";
        idColumn = "id";
        ownSearchableColumns = new String[] {
            "business_phone_area_code",
            "business_phone_number",
            "company",
            "position"
        };
        searchableColumns = getSearchableColumns();
        orderableColumns.add(new KeyValuePair<>(9, "company"));
        orderableColumns.add(new KeyValuePair<>(10, "position"));
    }
}

import ui.frames.BaseFrame;

public class Main
{
    /**
     * App's starting point
     *
     * @param args default arguments
     */
    public static void main(String[] args)
    {
        BaseFrame.main();
    }
}

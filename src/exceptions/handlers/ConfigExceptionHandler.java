package exceptions.handlers;

import javax.swing.*;

public class ConfigExceptionHandler
{
    /**
     * Show error dialog
     *
     * @param message error message
     */
    public static void showConfigExceptionDialog(String message)
    {
        JOptionPane.showMessageDialog(null, message, "Error!", JOptionPane.ERROR_MESSAGE);

        System.exit(1);
    }
}

package exceptions.handlers;

import javax.swing.*;

/**
 * Exception handler class
 */
public class DbExceptionHandler
{
    /**
     * Show error dialog
     *
     * @param message error message
     */
    public static void showDbExceptionDialog(String message)
    {
        JOptionPane.showMessageDialog(null, message, "Error!", JOptionPane.ERROR_MESSAGE);

        System.exit(1);
    }

    /**
     * Show error dialog
     *
     * @param message error message
     */
    public static void showQueryExceptionDialog(String message)
    {
        JOptionPane.showMessageDialog(null, message, "Error!", JOptionPane.ERROR_MESSAGE);
    }
}

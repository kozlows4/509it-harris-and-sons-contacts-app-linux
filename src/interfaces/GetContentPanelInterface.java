package interfaces;

import javax.swing.*;

public interface GetContentPanelInterface
{
    /**
     * Get content of the frame
     *
     * @return "content" JPanel
     */
    public JPanel getContent();
}

package ui.frames;

import lib.models.BusinessContact;
import lib.models.Model;
import lib.models.PersonalContact;
import lib.utils.KeyValuePair;
import ui.panels.*;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;
import java.util.List;

public class BaseFrame extends JFrame
{
    /**
     * Constants identifying panels and associated buttons
     */
    public static final String MAIN_MENU = "main_menu";
    public static final String BUSINESS_CONTACTS = "business_contacts";
    public static final String PERSONAL_CONTACTS = "personal_contacts";
    public static final String ADD_EDIT_BUSINESS_CONTACT = "add_edit_business_contact";
    public static final String ADD_EDIT_PERSONAL_CONTACT = "add_edit_personal_contact";

    /**
     * Frame components
     */
    private JButton mainMenuButton;
    private JButton businessContactsButton;
    private JButton personalContactsButton;
    private JButton addBusinessContactButton;
    private JButton addPersonalContactButton;
    private JPanel container;
    private JPanel formContainer;
    private JPanel mainMenuContent;
    private JPanel businessContactsListContent;
    private JPanel personalContactsListContent;
    private JPanel addEditBusinessContactContent;
    private JPanel addEditPersonalContactContent;

    /**
     * External frames providing togglable panels
     */
    private MainMenuFrame mainMenuFrame;
    private ContactsListFrame businessContactsFrame;
    private ContactsListFrame personalContactsFrame;
    private AddEditContactFrame addEditBusinessContactFrame;
    private AddEditContactFrame addEditPersonalContactFrame;

    /**
     * Cards container
     */
    private final CardLayout cardLayout = (CardLayout) (container.getLayout());

    /**
     * Main menu buttons
     */
    private final List<KeyValuePair<String, JButton>> buttons = new LinkedList<>();

    /**
     * Database access models
     */
    private final Model businessContact = new BusinessContact();
    private final Model personalContact = new PersonalContact();

    /**
     * Constructor
     */
    public BaseFrame()
    {
        // Add panels to CardLayout container
        container.add(mainMenuContent, MAIN_MENU);
        container.add(businessContactsListContent, BUSINESS_CONTACTS);
        container.add(personalContactsListContent, PERSONAL_CONTACTS);
        container.add(addEditBusinessContactContent, ADD_EDIT_BUSINESS_CONTACT);
        container.add(addEditPersonalContactContent, ADD_EDIT_PERSONAL_CONTACT);

        // Configure base frame
        this.setTitle("Harris & Sons Consulting LTD - Contact Management App");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setContentPane(formContainer);
        this.pack();

        // Configure togglable frames
        mainMenuFrame.setBaseFrame(this);
        // Business contacts list
        businessContactsFrame.setBaseFrame(this);
        businessContactsFrame.setAddEditForm(addEditBusinessContactFrame);
        businessContactsFrame.setContactModel(businessContact);
        // Personal contacts list
        personalContactsFrame.setBaseFrame(this);
        personalContactsFrame.setAddEditForm(addEditPersonalContactFrame);
        personalContactsFrame.setContactModel(personalContact);
        // Add edit business contact
        addEditBusinessContactFrame.setBaseFrame(this);
        addEditBusinessContactFrame.setContactsList(businessContactsFrame);
        addEditBusinessContactFrame.setContactModel(businessContact);
        // Add edit personal contact
        addEditPersonalContactFrame.setBaseFrame(this);
        addEditPersonalContactFrame.setContactsList(personalContactsFrame);
        addEditPersonalContactFrame.setContactModel(personalContact);

        // Add buttons to an iterable list
        buttons.add(new KeyValuePair<>(MAIN_MENU, mainMenuButton));
        buttons.add(new KeyValuePair<>(BUSINESS_CONTACTS, businessContactsButton));
        buttons.add(new KeyValuePair<>(PERSONAL_CONTACTS, personalContactsButton));
        buttons.add(new KeyValuePair<>(ADD_EDIT_BUSINESS_CONTACT, addBusinessContactButton));
        buttons.add(new KeyValuePair<>(ADD_EDIT_PERSONAL_CONTACT, addPersonalContactButton));

        // Click action listeners to buttons
        mainMenuButton.addActionListener(e -> mainMenuFrame.open());
        businessContactsButton.addActionListener(e -> businessContactsFrame.open());
        personalContactsButton.addActionListener(e -> personalContactsFrame.open());
        addBusinessContactButton.addActionListener(e -> addEditBusinessContactFrame.open());
        addPersonalContactButton.addActionListener(e -> addEditPersonalContactFrame.open());

        mainMenuFrame.open();
    }

    /**
     * Switch displayed frame
     *
     * @param panel constant corresponding to the panel
     */
    public void togglePanel(String panel)
    {
        togglePanel(panel, false);
    }

    /**
     * Switch displayed frame
     *
     * @param panel constant corresponding to the panel
     */
    public void togglePanel(String panel, boolean resetMainMenu)
    {
        cardLayout.show(container, panel);

        if (resetMainMenu) {
            for (KeyValuePair<String, JButton> button : buttons) {
                button.getValue().setBackground(Color.white);
            }
        } else {
            for (KeyValuePair<String, JButton> button : buttons) {
                if (button.getKey().equals(panel)) {
                    button.getValue().setBackground(Color.decode("#6495ED"));
                } else {
                    button.getValue().setBackground(Color.white);
                }
            }
        }
    }

    /**
     * Update main frame's title
     *
     * @param title new title
     */
    public void setFormTitle(String title)
    {
        this.setTitle(title);
    }

    /**
     * Create and display form
     */
    public static void main()
    {
        BaseFrame form = new BaseFrame();
        form.setVisible(true);
    }

    private void createUIComponents()
    {
        mainMenuFrame = new MainMenuFrame();
        businessContactsFrame = new BusinessContactsFrame();
        personalContactsFrame = new PersonalContactsFrame();
        addEditBusinessContactFrame = new AddEditBusinessContactFrame();
        addEditPersonalContactFrame = new AddEditPersonalContactFrame();

        mainMenuContent = mainMenuFrame.getContent();
        businessContactsListContent = businessContactsFrame.getContent();
        personalContactsListContent = personalContactsFrame.getContent();
        addEditBusinessContactContent = addEditBusinessContactFrame.getContent();
        addEditPersonalContactContent = addEditPersonalContactFrame.getContent();
    }
}

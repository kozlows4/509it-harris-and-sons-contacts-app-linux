package ui.panels;

import lib.tables.models.BusinessContactsTableModel;
import lib.tables.models.ContactsListTableModel;
import lib.tables.renderers.WrappedTextCellRenderer;
import ui.frames.BaseFrame;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;

import java.util.stream.IntStream;

import static javax.swing.ListSelectionModel.SINGLE_SELECTION;

public class BusinessContactsFrame extends ContactsListFrame
{
    private JPanel content;
    private JComboBox<Integer> paginateSelect;
    private JTextField contactsSearch;
    private JTable contactsList;
    private JButton editButton;
    private JButton deleteButton;
    private JButton nextButton;
    private JButton prevButton;
    private JLabel pageNumber;

    /**
     * Sortable columns
     */
    private static final int[] sortableColumns = {
        0,
        1,
        2,
        9,
        10
    };

    /**
     * Constructor
     */
    public BusinessContactsFrame()
    {
        editButton.addActionListener(this::editButtonClicked);
        deleteButton.addActionListener(this::deleteButtonClicked);
        nextButton.addActionListener(this::nextButtonClicked);
        prevButton.addActionListener(this::prevButtonClicked);
        paginateSelect.addActionListener(this::perPageNumberChanged);

        contactsListTable = contactsList;
        contactsSearchField = contactsSearch;
        paginationSelectDropdown = paginateSelect;
        pageNumberLabel = pageNumber;
        prevPageButton = prevButton;
        nextPageButton = nextButton;
    }

    @Override
    public JPanel getContent()
    {
        return content;
    }

    @Override
    public void togglePanel()
    {
        baseFrame.togglePanel(BaseFrame.BUSINESS_CONTACTS);
        baseFrame.setFormTitle("Harris & Sons Consulting LTD - Business Contacts");
    }

    @Override
    protected void initContactsTable()
    {
        ContactsListTableModel tableModel = new BusinessContactsTableModel();

        contactsList = new JTable(tableModel)
        {
            public TableCellRenderer getCellRenderer(int row, int col) {
                if (col == 4) {
                    return new WrappedTextCellRenderer();
                } else {
                    return new DefaultTableCellRenderer();
                }
            }
        };
        contactsList.setSelectionMode(SINGLE_SELECTION);

        TableRowSorter<ContactsListTableModel> sorter = new TableRowSorter<>((ContactsListTableModel) contactsList.getModel());

        for (int i = 0, j = tableModel.getColumnCount(); i < j; i++)
        {
            int finalI = i;
            sorter.setSortable(i, IntStream.of(sortableColumns).anyMatch(x -> x == finalI));
        }

        sorter.addRowSorterListener(this::sortingChanged);

        contactsList.setRowSorter(sorter);
    }

    private void createUIComponents()
    {
        paginateSelect = new JComboBox<>(paginationValues);
        contactsSearch = new JTextField();
        contactsSearch.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void changedUpdate(DocumentEvent e) {
                searchChanged();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                searchChanged();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                searchChanged();
            }
        });

        initContactsTable();
    }
}

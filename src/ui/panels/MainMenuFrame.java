package ui.panels;

import ui.frames.BaseFrame;

import javax.swing.*;

public class MainMenuFrame extends PanelContainerFrame
{
    private JPanel content;

    @Override
    public JPanel getContent()
    {
        return content;
    }

    /**
     * Show main menu panel
     */
    public void open()
    {
        baseFrame.togglePanel(BaseFrame.MAIN_MENU);
        baseFrame.setFormTitle("Harris & Sons Consulting LTD - Contact Management App");
    }
}

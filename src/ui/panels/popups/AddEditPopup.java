package ui.panels.popups;

import javax.swing.*;

public class AddEditPopup
{
    /**
     * Show contact not found dialog
     */
    public static void showContactNotFoundDialog()
    {
        JOptionPane.showMessageDialog(null, "Selected contact doesn't exist!", "Error!", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Show insert/update error dialog
     *
     * @param message message
     */
    public static void showInsertUpdateErrorDialog(String message)
    {
        JOptionPane.showMessageDialog(null, message, "Error!", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Show insert/update success dialog
     *
     * @param message message
     * @return response
     */
    public static int showInsertUpdateSuccessDialog(String message)
    {
        return JOptionPane.showConfirmDialog(
            null,
            message,
            "Success",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE
        );
    }
}

package ui.panels.popups;

import javax.swing.*;

public class ContactsTablePopup
{
    /**
     * Show warning dialog
     */
    public static void showRowNotSelectedDialog()
    {
        JOptionPane.showMessageDialog(null, "Please select a contact to continue", "Warning!", JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Confirm contact edit
     */
    public static boolean confirmEdit()
    {
        return JOptionPane.showConfirmDialog(
            null,
            "Do you want to edit the contact?",
            "Edit",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE
        ) == JOptionPane.YES_OPTION;
    }

    /**
     * Confirm contact delete
     */
    public static boolean confirmDelete()
    {
        return JOptionPane.showConfirmDialog(
            null,
            "Do you want to edit the contact?",
            "Edit",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE
        ) == JOptionPane.YES_OPTION;
    }
}

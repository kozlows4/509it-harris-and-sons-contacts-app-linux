package ui.panels;

import interfaces.GetContentPanelInterface;
import lib.models.Model;
import ui.frames.BaseFrame;

import javax.swing.*;

public abstract class PanelContainerFrame extends JFrame implements GetContentPanelInterface
{
    /**
     * Base form object
     */
    protected BaseFrame baseFrame;

    /**
     * Database access model
     */
    protected Model contactModel;

    /**
     * Set reference to main form
     *
     * @param baseFrame main form
     */
    public void setBaseFrame(BaseFrame baseFrame)
    {
        this.baseFrame = baseFrame;
    }

    /**
     * Set reference to the database model
     *
     * @param contactModel database model
     */
    public void setContactModel(Model contactModel)
    {
        this.contactModel = contactModel;
    }
}

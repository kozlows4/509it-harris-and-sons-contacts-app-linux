package ui.panels;

import lib.tables.helpers.TableRowsSettings;
import lib.tables.models.ContactsListTableModel;
import lib.utils.KeyValuePair;
import ui.panels.popups.ContactsTablePopup;

import javax.swing.*;
import javax.swing.event.RowSorterEvent;
import javax.swing.table.TableRowSorter;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public abstract class ContactsListFrame extends PanelContainerFrame
{
    /**
     * Add/Edit form reference
     */
    protected AddEditContactFrame addEditForm;

    protected final Integer[] paginationValues = {10, 25, 50, 100};

    /**
     * Frame components
     */
    protected JTable contactsListTable;
    protected JTextField contactsSearchField;
    protected JComboBox<Integer> paginationSelectDropdown;
    protected JLabel pageNumberLabel;
    protected JButton prevPageButton;
    protected JButton nextPageButton;

    /**
     * Sorting settings
     */
    private int sortingColumn = 0;
    private boolean sortAscending = true;

    /**
     * Set reference to add/edit form instance
     *
     * @param addEditForm form object
     */
    public void setAddEditForm(AddEditContactFrame addEditForm)
    {
        this.addEditForm = addEditForm;
    }

    /**
     * Overload for default value parameter
     */
    public void open()
    {
        open(true);
    }

    /**
     * Show contacts list
     *
     * @param resetPagination pagination reset flag
     */
    public void open(boolean resetPagination)
    {
        togglePanel();

        updateContactsListData(resetPagination);
    }

    /**
     * Toggle displayed panel and form's title
     */
    protected abstract void togglePanel();

    /**
     * Configure contacts table
     */
    protected abstract void initContactsTable();

    /**
     * Sorting changed callback
     *
     * @param evt event
     */
    protected void sortingChanged(RowSorterEvent evt)
    {
        if (evt.getType() == RowSorterEvent.Type.SORT_ORDER_CHANGED) {
            RowSorter.SortKey key = evt.getSource().getSortKeys().get(0);

            sortingColumn = key.getColumn();
            sortAscending = (key.getSortOrder() == SortOrder.ASCENDING);

            updateContactsListData(true);
        }
    }

    /**
     * Search input text change callback
     */
    protected void searchChanged()
    {
        updateContactsListData(true);
    }

    /**
     * Pagination limit change callback
     *
     * @param event event
     */
    protected void perPageNumberChanged(ActionEvent event)
    {
        updateContactsListData(true);
    }

    /**
     * Edit button click callback
     *
     * @param event event
     */
    protected void editButtonClicked(ActionEvent event)
    {
        int selectedId = getSelectedRowId();

        if (selectedId > 0) {
            if (ContactsTablePopup.confirmEdit()) {
                addEditForm.open(selectedId);
            }
        } else {
            ContactsTablePopup.showRowNotSelectedDialog();
        }
    }

    /**
     * Delete button click callback
     *
     * @param event event
     */
    protected void deleteButtonClicked(ActionEvent event)
    {
        int selectedId = getSelectedRowId();

        if (selectedId > 0) {
            if (
                ContactsTablePopup.confirmDelete()
                && contactModel.delete(selectedId)
            ) {
                updateContactsListData();
            }
        } else {
            ContactsTablePopup.showRowNotSelectedDialog();
        }
    }

    /**
     * Pagination control click callback
     *
     * @param event event
     */
    protected void prevButtonClicked(ActionEvent event)
    {
        int currentPage = getCurrentPageNumber();

        prevPageButton.setEnabled(--currentPage > 1);
        nextPageButton.setEnabled(true);

        pageNumberLabel.setText(String.valueOf(currentPage));

        updateContactsListData();
    }

    /**
     * Pagination control click callback
     *
     * @param event event
     */
    protected void nextButtonClicked(ActionEvent event)
    {
        pageNumberLabel.setText(String.valueOf(getCurrentPageNumber() + 1));

        prevPageButton.setEnabled(true);

        updateContactsListData();
    }

    /**
     * Overload with default value to update displayed contacts
     */
    private void updateContactsListData()
    {
        updateContactsListData(false);
    }

    /**
     * Display contacts
     *
     * @param resetPagination indicate if first page should be displayed
     */
    private void updateContactsListData(boolean resetPagination)
    {
        if (resetPagination) {
            resetPagination();
        }

        ContactsListTableModel model = (ContactsListTableModel) contactsListTable.getModel();

        int limit = getCurrentPerPageLimit();
        int offset = (getCurrentPageNumber() - 1) * limit;

        TableRowsSettings rowsSettings = model.updateData(
            contactModel.get(
                getSearchText(),
                limit + 1,
                offset,
                sortingColumn,
                sortAscending
            ),
            limit
        );

        updateRowsHeight(rowsSettings.rowsHeightSettings);

        updateNextPageStatus(
            rowsSettings.hasNextPage()
        );
    }

    /**
     * Change rows height to accommodate multi-line addresses
     *
     * @param rowsHeightSettings rows settings
     */
    private void updateRowsHeight(ArrayList<KeyValuePair<Integer, Integer>> rowsHeightSettings)
    {
        for (KeyValuePair<Integer, Integer> row : rowsHeightSettings) {
            contactsListTable.setRowHeight(row.getKey(), row.getValue());
        }
    }

    /**
     * Toggle next page pagination button
     *
     * @param enable toggle status
     */
    private void updateNextPageStatus(boolean enable)
    {
        nextPageButton.setEnabled(enable);
    }

    /**
     * Reset pagination controls
     */
    private void resetPagination()
    {
        prevPageButton.setEnabled(false);
        nextPageButton.setEnabled(false);
        pageNumberLabel.setText("1");
    }

    /**
     * Get current page number
     *
     * @return page number
     */
    private int getCurrentPageNumber()
    {
        return Integer.parseInt(pageNumberLabel.getText());
    }

    /**
     * Get maximum number of items to be displayed
     *
     * @return number of items per page
     */
    private int getCurrentPerPageLimit()
    {
        return Integer.parseInt(
            String.valueOf(
                paginationSelectDropdown.getSelectedItem()
            )
        );
    }

    /**
     * Get id of the selected contact
     *
     * @return id of the selected contact
     */
    private int getSelectedRowId()
    {
        int[] selectedRows = contactsListTable.getSelectedRows();

        if (selectedRows.length < 1) {
            return 0;
        } else {
            return ((ContactsListTableModel) contactsListTable.getModel()).getRowId(selectedRows[0]);
        }
    }

    /**
     * Get search text
     *
     * @return search text
     */
    private String getSearchText()
    {
        return contactsSearchField.getText();
    }
}

package ui.panels;

import lib.db.QueryResult;
import lib.utils.KeyValuePair;
import ui.panels.popups.AddEditPopup;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class AddEditContactFrame extends PanelContainerFrame
{
    /**
     * ID of edited contact
     */
    private int contactId = 0;

    /**
     * Contacts list reference
     */
    protected ContactsListFrame contactsList;

    /**
     * Form inputs
     */
    protected ArrayList<KeyValuePair<String, JTextField>> inputs = new ArrayList<>();
    protected ArrayList<KeyValuePair<String, Integer>> inputsMaxLength = new ArrayList<>();
    protected String[] nullableInputs = {
        "address_line_2",
        "address_line_3",
    };

    /**
     * Submit button
     */
    protected JButton formSubmitButton;

    /**
     * Constructor
     */
    public AddEditContactFrame()
    {
        inputsMaxLength.add(new KeyValuePair<>("first_name", 50));
        inputsMaxLength.add(new KeyValuePair<>("last_name", 50));
        inputsMaxLength.add(new KeyValuePair<>("phone_area_code", 5));
        inputsMaxLength.add(new KeyValuePair<>("phone_number", 20));
        inputsMaxLength.add(new KeyValuePair<>("postcode", 10));
    }

    /**
     * Set reference to contacts list instance
     *
     * @param contactsList form object
     */
    public void setContactsList(ContactsListFrame contactsList)
    {
        this.contactsList = contactsList;
    }

    /**
     * Overload for default value parameter
     */
    public void open()
    {
        open(0);
    }

    /**
     * Show add/edit form
     *
     * @param id contact id
     */
    public void open(int id)
    {
        contactId = id;

        togglePanel(isEdit());

        populateForm();
    };

    /**
     * Toggle displayed panel and form's title
     *
     * @param isEdit add/edit flag
     */
    protected abstract void togglePanel(boolean isEdit);

    /**
     * Submit button click callback
     *
     * @param event event
     */
    protected void submitButtonClicked(ActionEvent event)
    {
        if (!validateForm()) {
            return;
        }

        if (isEdit()) {
            updateContact();
        } else {
            insertContact();
        }
    }

    /**
     * Cancel button click callback
     *
     * @param event event
     */
    protected void cancelButtonClicked(ActionEvent event)
    {
        contactsList.open(
            !isEdit()
        );
    }

    /**
     * Check if edit form is opened
     *
     * @return flag indicating if edit form is opened
     */
    private boolean isEdit()
    {
        return contactId > 0;
    }

    /**
     * Reset form overload
     */
    private void resetForm()
    {
        resetForm(false);
    }

    /**
     * Reset form
     *
     * @param resetSelectedContactId indicate if ID of selected contact should be cleared
     */
    private void resetForm(boolean resetSelectedContactId)
    {
        formSubmitButton.setEnabled(true);

        if (resetSelectedContactId) {
            contactId = 0;
        }

        for (KeyValuePair<String, JTextField> input : inputs) {
            JTextField inputField = input.getValue();

            inputField.setText("");
            inputField.setToolTipText("");
            inputField.setBackground(Color.WHITE);
        }
    }

    /**
     * Populate form
     */
    private void populateForm()
    {
        resetForm();

        if (contactId == 0) {
            return;
        }

        QueryResult contact = contactModel.find(contactId);

        if (contact.rows.size() < 1) {
            contactId = 0;

            formSubmitButton.setEnabled(false);

            AddEditPopup.showContactNotFoundDialog();
        } else {
            setInputValues(contact);
        }

        System.out.println(contact.rows.size());
    }

    /**
     * Set input values
     *
     * @param contact selected contact
     */
    private void setInputValues(QueryResult contact)
    {
        String[] contactData = contact.rows.get(0);

        for (int i = 0; i < contact.columnsCount; i++) {
            setInputValue(contact.columnNames[i], contactData[i]);
        }
    }

    /**
     * Set value of specified input
     *
     * @param inputName input name
     * @param value value
     */
    private void setInputValue(String inputName, String value)
    {
        for (KeyValuePair<String, JTextField> input : inputs) {
            if (input.getKey().equals(inputName)) {
                input.getValue().setText(value);

                break;
            }
        }
    }

    /**
     * Validate form
     *
     * @return validation result
     */
    private boolean validateForm()
    {
        boolean isValidForm = true;
        boolean isValidInput;
        String message = "";

        for (KeyValuePair<String, JTextField> input : inputs) {
            isValidInput = true;
            message = "";

            JTextField inputField = input.getValue();
            String inputName = input.getKey();
            String inputValue = inputField.getText();

            if (inputValue.equals("")) {
                if (!isNullable(inputName)) {
                    isValidInput = false;
                    message = "Field mustn't be empty!";
                }
            } else {
                int maxLength = getMaximumLength(inputName);

                if (inputValue.length() > maxLength) {
                    isValidInput = false;
                    message = "Entered value mustn't be longer than " + maxLength + " characters!";
                } else if (inputName.equals("email") && !isValidEmail(inputValue)) {
                    isValidInput = false;
                    message = "Entered value must be a valid email address!";
                }
            }

            if (isValidInput) {
                inputField.setToolTipText("");
                inputField.setBackground(Color.white);
            } else {
                inputField.setToolTipText(message);
                inputField.setBackground(Color.decode("#ff8d85"));

                isValidForm = false;
            }
        }

        return isValidForm;
    }

    /**
     * Insert contact
     */
    private void insertContact()
    {
        if (contactModel.insert(getFormData())) {
            if (
                AddEditPopup.showInsertUpdateSuccessDialog(
                    "Contact has been added. Do you want to add another contact?"
                ) == JOptionPane.YES_OPTION
            ) {
                resetForm(true);
            } else {
                contactsList.open();
            }
        } else {
            AddEditPopup.showInsertUpdateErrorDialog("Contact couldn't be added!");
        }
    }

    /**
     * Update contact
     */
    private void updateContact()
    {
        if (contactModel.update(contactId, getFormData())) {
            if (
                AddEditPopup.showInsertUpdateSuccessDialog(
                    "Contact has been Updated. Do you want to make any other changes?"
                ) == JOptionPane.NO_OPTION
            ) {
                contactsList.open(false);
            }
        } else {
            AddEditPopup.showInsertUpdateErrorDialog("Contact update failed!");
        }
    }

    /**
     * Form data
     *
     * @return form data
     */
    private ArrayList<KeyValuePair<String, Object>> getFormData()
    {
        ArrayList<KeyValuePair<String, Object>> formData = new ArrayList<>(inputs.size());

        for (KeyValuePair<String, JTextField> input : inputs) {
            String value = input.getValue().getText();

            formData.add(
                new KeyValuePair<>(
                    input.getKey(),
                    (value.length() > 0) ? value : null
                )
            );
        }

        return formData;
    }

    /**
     * Check if input can be empty
     *
     * @param inputName input name
     * @return flag indicating if input may be empty
     */
    private boolean isNullable(String inputName)
    {
        return Arrays.asList(nullableInputs).contains(inputName);
    }

    /**
     * Get maximum length
     *
     * @param inputName input name
     * @return maximum length
     */
    private int getMaximumLength(String inputName)
    {
        int maxLength = 255;

        for (KeyValuePair<String, Integer> maxInputLength : inputsMaxLength)
        {
            if (maxInputLength.getKey().equals(inputName)) {
                maxLength = maxInputLength.getValue();

                break;
            }
        }

        return maxLength;
    }

    /**
     * Validate email address
     *
     * @param email email address
     * @return validation result
     */
    private boolean isValidEmail(String email)
    {
        boolean result = true;

        try {
            new InternetAddress(email).validate();
        } catch (AddressException ex) {
            result = false;
        }

        return result;
    }
}

package ui.panels;

import lib.utils.KeyValuePair;
import ui.frames.BaseFrame;

import javax.swing.*;

public class AddEditBusinessContactFrame extends AddEditContactFrame
{
    private JPanel content;
    private JTextField firstName;
    private JTextField lastName;
    private JTextField email;
    private JTextField phoneAreaCode;
    private JTextField phoneNumber;
    private JTextField addressLine1;
    private JTextField businessPhoneAreaCode;
    private JTextField addressLine2;
    private JTextField addressLine3;
    private JTextField postcode;
    private JTextField city;
    private JTextField country;
    private JTextField businessPhoneNumber;
    private JTextField company;
    private JTextField position;
    private JButton submitButton;
    private JButton cancelButton;

    /**
     * Constructor
     */
    public AddEditBusinessContactFrame()
    {
        submitButton.addActionListener(this::submitButtonClicked);
        cancelButton.addActionListener(this::cancelButtonClicked);

        formSubmitButton = submitButton;

        inputs.add(new KeyValuePair<>("first_name", firstName));
        inputs.add(new KeyValuePair<>("last_name", lastName));
        inputs.add(new KeyValuePair<>("email", email));
        inputs.add(new KeyValuePair<>("phone_area_code", phoneAreaCode));
        inputs.add(new KeyValuePair<>("phone_number", phoneNumber));
        inputs.add(new KeyValuePair<>("address_line_1", addressLine1));
        inputs.add(new KeyValuePair<>("address_line_2", addressLine2));
        inputs.add(new KeyValuePair<>("address_line_3", addressLine3));
        inputs.add(new KeyValuePair<>("postcode", postcode));
        inputs.add(new KeyValuePair<>("city", city));
        inputs.add(new KeyValuePair<>("country", country));
        inputs.add(new KeyValuePair<>("business_phone_area_code", businessPhoneAreaCode));
        inputs.add(new KeyValuePair<>("business_phone_number", businessPhoneNumber));
        inputs.add(new KeyValuePair<>("company", company));
        inputs.add(new KeyValuePair<>("position", position));

        inputsMaxLength.add(new KeyValuePair<>("business_phone_area_code", 5));
        inputsMaxLength.add(new KeyValuePair<>("business_phone_number", 20));
    }

    @Override
    public JPanel getContent()
    {
        return content;
    }

    @Override
    protected void togglePanel(boolean isEdit)
    {
        baseFrame.togglePanel(BaseFrame.ADD_EDIT_BUSINESS_CONTACT);
        baseFrame.setFormTitle(
            "Harris & Sons Consulting LTD - " +
            (isEdit ? "Edit" : "Add") +
            " Business Contact"
        );
    }
}

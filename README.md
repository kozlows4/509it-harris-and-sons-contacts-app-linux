Harris & Sons Contact Management App - Java
---
Last updated: 13 December 2020

Author: [Jakub Kozlowski](kozlows4@cucollege.coventry.ac.uk)

Module: 509IT - Advanced Software Development


## Description

Java-based implementation of the contact management app. 

### Development Environment
#### Required software 
1. Java
2. XAMPP (or equivalent software including MySQL database server)
3. IDE of choice
#### Setup
1. Import or create a database
2. Copy "/config/config.properties.example" file to "/config/config.properties" and enter local configuration for database access